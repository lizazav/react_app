let initialState={
  id:0
};
export default function (state = initialState, action) {
  console.log(action);
  switch (action.type) {
    case  'HandleDelete':
      return state.filter(({ id }) => id !== action.payload);
  }
  return state;
}