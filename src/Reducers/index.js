import {combineReducers} from "redux";
import ActiveTasksReducers from "./ActiveTasks";
import DoneTasksReducers from "./DoneTasks";
import Tasks from "./Tasks";
const allReducers=combineReducers({
  Active:ActiveTasksReducers,
  Done: DoneTasksReducers,
  Tasks:Tasks
});
export default allReducers;