import React from "react";
import "./css/App.css";
import {connect} from "react-redux";
import Task from "./components/Task/Task";
import AddInput from "./components/AddInput/AddInput";
const App=(props)=> {

//   handleSubmit = value => {
//     this.setState(state => {
//       let { tasks } = state;
//       tasks.push({
//         id: tasks.length,
//         title: value,
//         isDone: false
//       });
//       return tasks;
//     });
//   };
//   handleUncomplete = id => {
//     this.setState(state => {
//       let { tasks } = state;
//       tasks[id].isDone = false;
//       return tasks;
//     });
//   };
//   handleComplete = id => {
//     this.setState(state => {
//       let { tasks } = state;
//       tasks[id].isDone = true;
//       return tasks;
//     });
//   };
//   handleDelete = id => {
//     this.setState(state => {
//       let { tasks } = state;
//       delete tasks[id];
//       return tasks;
//     });
//   };






    return (
      <div>
        <div className="title">
          <h1>TEMPLATE</h1>
        </div>
        <div className="tasks-list">
          <div className="tasks-list__active">
            <h2>Active Tasks:</h2>

            <span>
                <Task />



            </span>
          </div>

          <div className="tasks-list__done">
            <h2>Done Tasks:</h2>
            <span>

            </span>
          </div>
        </div>
        {/*<AddInput handleSubmit={this.handleSubmit} />*/}
      </div>

    );

};
// class App extends React.Component {
//   state = {
//     tasks: [
//       { id: 0, title: "task1", isDone: false },
//       { id: 1, title: "task2", isDone: true },
//       { id: 2, title: "task3", isDone: true }
//     ]
//   };
//   handleSubmit = value => {
//     this.setState(state => {
//       let { tasks } = state;
//       tasks.push({
//         id: tasks.length,
//         title: value,
//         isDone: false
//       });
//       return tasks;
//     });
//   };
//   handleUncomplete = id => {
//     this.setState(state => {
//       let { tasks } = state;
//       tasks[id].isDone = false;
//       return tasks;
//     });
//   };
//   handleComplete = id => {
//     this.setState(state => {
//       let { tasks } = state;
//       tasks[id].isDone = true;
//       return tasks;
//     });
//   };
//   handleDelete = id => {
//     this.setState(state => {
//       let { tasks } = state;
//       delete tasks[id];
//       return tasks;
//     });
//   };
//
//   render() {
//     const { tasks } = this.state;
//     const activeTasks = tasks.filter(task => !task.isDone);
//     const isDoneTasks = tasks.filter(task => task.isDone);
//     return (
//       <div>
//         <div className="title">
//           <h1>TEMPLATE</h1>
//         </div>
//         <div className="tasks-list">
//           <div className="tasks-list__active">
//             <h2>Active Tasks:{activeTasks.length}</h2>
//
//             <span>
//               {activeTasks.map(task => (
//                 <Task
//                   handleComplete={() => this.handleComplete(task.id)}
//                   handleDelete={() => this.handleDelete(task.id)}
//                   task={task}
//                   key={task.id}
//                 />
//               ))}
//             </span>
//           </div>
//
//           <div className="tasks-list__done">
//             <h2>Done Tasks:{isDoneTasks.length}</h2>
//             <span>
//               {isDoneTasks.map(task => (
//                 <Task
//                   handleDelete={() => this.handleDelete(task.id)}
//                   handleUncomplete={() => this.handleUncomplete(task.id)}
//                   task={task}
//                   key={task.id}
//                 />
//               ))}
//             </span>
//           </div>
//         </div>
//         <AddInput handleSubmit={this.handleSubmit} />
//       </div>
//     );
//   }
// }
export default connect (
  state =>({
    store: state
  }),
  dispatch => ({})
)(App);
