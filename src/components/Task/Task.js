import React from "react";
import "./Task.css";
import {connect} from "react-redux";
const Task = (props) => {
  const showTasks=()=>{
    return props.store.Active.map((Tasks)=>{
      return(
        <div className="task">
        <span  key={Tasks.id} className="text">{Tasks.title}</span>
        <ActionButton handleDelete={() => handleDelete(Tasks.id)}/>
      </div>)
    })
  };
  const  handleDelete = id => {
    return{
      type:'HandleDelete',
      payload:{id}
    }
  };

  const ActionButton = () => (
    <div className="task_button">

        <p >&#10004;</p>

      <p onClick={ handleDelete }>delete</p>
    </div>
  );

  return (
        showTasks()
  );
};
export default connect (
  state =>({
    store: state
  }),
  dispatch => ({})
)(Task);

