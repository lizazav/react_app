import React from "react";
import "./AddInput.css";
class AddInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
  }

  handleSubmit = event => {
    if (this.state.value) {
      this.props.handleSubmit(this.state.value);
      this.setState({ value: "" });
    }
    event.preventDefault();
  };
  handleChange = event => {
    this.setState({ value: event.target.value });
  };
  render() {
    return (
      <div className="add-task">
        <h1>Add a task</h1>
        <form onSubmit={this.handleSubmit}>
          <label>Enter the task</label>
          <input
            onChange={this.handleChange}
            className="add-task__input"
            value={this.state.value}
          />
          <button>ADD</button>
        </form>
      </div>
    );
  }
}
export default AddInput;
